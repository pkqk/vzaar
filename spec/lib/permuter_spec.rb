require 'spec_helper'
require 'permuter'

describe Permuter do
  it 'takes a list of words on new' do
    Permuter.new(%w[list of words])
  end
  
  it 'should list any words that are a permutation of the search term' do
    Permuter.new(%w[pots pans opts]).words_from("stop").should == Set.new(%w[pots opts])
  end
  
  it 'should load a text file of words and create a Permuter' do
    permuter = Permuter.load(StringIO.new("several\nwords\nin\na\nfile"))
    permuter.words_from("file").should == Set.new(%w[file])
  end
  
  it 'should compute the search time' do
    permuter = Permuter.new(%w[pots pans opts])
    permuter.words_from("stop")
    permuter.last_search_time.should be_a(Float)
  end

end