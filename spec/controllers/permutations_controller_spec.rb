require 'spec_helper'

describe PermutationsController do
  render_views

  describe "GET 'index'" do
    before { get :index }

    it "returns http success" do
      response.should be_success
    end
    
  end
  
  describe 'POST upload' do
    it "should take a dictionary file to create a permuter with" do
      upload = Rack::Test::UploadedFile.new(Rails.root+'lib/dictionary.txt', "text/plain")
      upload.stub(:tempfile => :datafile)
      Permuter.should_receive(:load).with(:datafile, 'dictionary.txt').and_return(double(:id => 1))
      post :upload, :dictionary => upload
    end
  end

  describe "GET 'search'" do
    it "returns 404 for empty search" do
      get :search
      response.code.should eq("404")
    end

    it "returns 404 for search without a dictionary" do
      get :search, :q => 'word'
      response.code.should eq("404")
    end
    
    describe 'with a loaded permuter' do
      before do
        permuter = double('permuter')
        permuter.stub(
          :words_from => Set.new(%w[pots stop]),
          :last_search_time => 33.3
        )
        controller.stub(:permuter).and_return(permuter)
      end

      it 'returns 200 on successful search' do
        get :search, :q => 'stop', :format => :json
        response.code.should eq("200")
      end
    end
  end

end
