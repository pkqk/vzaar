require 'set'

class Permuter
  attr_accessor :dictionary, :name

  def initialize(dictionary, name='dictionary')
    @dictionary = Set.new(dictionary)
    @name = name
  end
  
  def words_from(search_term)
    if search_term.present?
      @start = Time.now
      result = dictionary & permutations_from(search_term)
      @end = Time.now
      result
    end
  end
  
  def last_search_time
    @end - @start if @end && @start
  end
  
  def id
    @id ||= "#{name}-#{rand(99999)}"
  end
  
  def permutations_from(word)
    letters = word.each_char.to_a
    letters.permutation.to_a.map(&:join)
  end
  
  def self.load(file, name=nil)
    words = Set.new
    file.each do |line|
      words.add(line.strip) if line.present?
    end
    new(words, name)
  end
end