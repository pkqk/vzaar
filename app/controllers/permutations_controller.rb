require 'benchmark'
require 'permuter'

class PermutationsController < ApplicationController
  include PermutationsHelper
  attr_accessor :search_results, :search_time
  helper_method :search_results, :search_time

  def index
  end

  def search
    head :not_found and return unless permuter

    @search_results = permuter.words_from(params[:q])
    @search_time = permuter.last_search_time
  end
  
  def upload
    self.permuter = Permuter.load(params[:dictionary].tempfile, params[:dictionary].original_filename)
    redirect_to root_path
  end
  
end
