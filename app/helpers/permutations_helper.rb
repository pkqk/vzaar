module PermutationsHelper
  def permuter_loaded?
    session[:permuter_id].present? && permuter
  end
  
  def permuter_name
    permuter.name if permuter_loaded?
  end

protected
  def permuter
    if id = session[:permuter_id]
      (Thread.current[:permuters] ||= {})[id]
    end
  end

  def permuter=(permuter)
    session[:permuter_id] = permuter.id
    (Thread.current[:permuters] ||= {})[permuter.id] = permuter
  end

end
